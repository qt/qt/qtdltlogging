// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR GFDL-1.3-no-invariants-only
/*!
    \page qtdltlogging-index.html
    \title Qt DLT Logging
    \keyword QtDltLogging

    The Qt DLT Logging module provides C++ classes for interacting with
    services from the \l {https://www.genivi.org/}{GENIVI Automotive Alliance}.
    The module contains helper functions to interact with the
    Diagnostic Log and Trace (DLT) daemon. This daemon is used on many
    automotive systems to catch all logs from different application.

    \section1 Getting Started

    To include the definitions of the module's classes and functions, use the
    following directive:

    \code
    #include <QtDltLogging>
    \endcode

    To link against the module, add this line to your qmake \c .pro file:

    \badcode
    QT += dltlogging
    \endcode

    See \l {Qt DLT Declarations} for more information about how to use the DLT API.

    \section1 Reference

    \list
      \li \l {Qt DLT Logging C++ Classes}{C++ Classes}
      \li \l {Qt DLT Logging Examples}{Examples}
    \endlist
*/

/*!
    \module QtDltLogging
    \title Qt DLT Logging C++ Classes
    \ingroup modules
    \qtvariable dltlogging

    \brief C++ classes for the Qt DLT Logging API.

    The Qt DLT Logging C++ API provides functions to interact with GENIVI services.

    \section1 Headers

    \annotatedlist dltlogging_headers
*/

/*!
    \headerfile <QtDlt>
    \title Qt DLT Declarations
    \target Qt DLT Declarations
    \ingroup dltlogging_headers
    \inmodule QtDltLogging

    \brief The <QtDlt> header file includes macros to register with a
    dlt-daemon and map \l {QLoggingCategory}{QLoggingCategories} to dlt contexts.

    \section1 GENIVI DLT
    GENIVI DLT is a logging system for automotive. It consists of two applications;
    a logging daemon (dlt-daemon) to collect logs of various applications and the linux system,
    and a viewer application (dlt-viewer) that runs on the host and can connect to targets running a dlt-daemon.

    The dlt-daemon can be configured to log to a file, or forward logs to another dlt-daemon in the
    multiple ECU case. For further information about the dlt-daemon, see the dlt-daemon
    \l {https://at.projects.genivi.org/wiki/display/PROJ/Diagnostic+Log+and+Trace} {documentation}.

    \section2 DLT Applications, Contexts, and Log Level

    Every application that wants to log to the dlt-daemon needs to register itself.
    Applications are identified by a four characters long identifier and a longer description for the dlt-viewer.

    Every registered application can register multiple dlt contexts, which consist of a four characters long
    identifier and a comprehensive description.

    Every dlt context has a current log level, which defines which logs are forwarded to the dlt-daemon or are
    discarded already in the application.

    \section2 Controlling the dlt-daemon

    The dlt-daemon is controlled by the dlt-viewer running on a host machine. The dlt-viewer can see all the
    applications that are registered to the dlt-daemon, along with their contexts and current log levels.
    All logs are forwarded from the dlt-daemon to the connected dlt-viewer, and can be filtered and exported.

    In addition, the dlt-viewer can send so called "control messages" to the dlt-daemon to change the log level of
    a dlt context. Beginning with dlt-daemon version 2.12, these "control messages" are also forwarded to the logging
    application and is parsed by Qt to also set the matching log level of the registered QLoggingCategory.

    \section1 Qt Message Type Mapping
    \target QDLTMessageTypeMapping

    As the DLT system isn't a one to one match to the Qt Categorized Logging system, a mapping of the log levels is needed.
    With DLT, it's not possible to only enable a specific message type of a QLoggingCategory as DLT only defines the maximum
    log level. Because of this limitation all lower message types are automatically enabled once a dlt contexts log level is changed.

    The following mapping is used for logging to DLT:

    \table
    \header \li Qt Message Type \li DLT log level
    \row \li QtDebugMsg \li DLT_LOG_DEBUG
    \row \li QtInfoMsg \li DLT_LOG_INFO
    \row \li QtWarningMsg \li DLT_LOG_WARN
    \row \li QtCriticalMsg \li DLT_LOG_ERROR
    \row \li QtFatalMsg \li DLT_LOG_FATAL
    \row \li No message type enabled \li DLT_LOG_OFF
    \endtable

    Changing the log level using dlt-viewer enables the following message types:

    \table
    \header \li DLT log level \li Qt Message Types
    \row \li DLT_LOG_VERBOSE \li QtDebugMsg QtInfoMsg QtWarningMsg QtCriticalMsg QtFatalMsg
    \row \li DLT_LOG_INFO \li QtDebugMsg QtInfoMsg QtWarningMsg QtCriticalMsg QtFatalMsg
    \row \li DLT_LOG_DEBUG \li QtInfoMsg QtWarningMsg QtCriticalMsg QtFatalMsg
    \row \li DLT_LOG_WARN \li QtWarningMsg QtCriticalMsg QtFatalMsg
    \row \li DLT_LOG_ERROR \li QtCriticalMsg QtFatalMsg
    \row \li DLT_LOG_FATAL \li QtFatalMsg
    \row \li DLT_LOG_OFF \li No message type enabled
    \endtable

    \section1 Disabling DLT
    If you are running unit tests, you most likely do not want DLT messages to go out, or you
    might not even have a DLT daemon running.

    You do not have to make code changes in scenarios like this, as you can simply tell Qt to not
    contact the DLT daemon at all: just export the environment variable \c QT_NO_DLT_LOGGING=1.
*/

/*!
    \macro QDLT_REGISTER_APPLICATION(NAME, DESCRIPTION)
    \relates <QtDlt>

    Registers an application with the dlt-daemon as \a NAME and \a DESCRIPTION.
    All registered DLT Logging Categories will be part of this application in the
    dlt-daemon.
    Only one application can be registered per process.

    \note \a NAME is limited to (a size of) 4 characters.
*/

/*!
    \macro QDLT_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME, DLT_CTX_NAME, DLT_CTX_DESCRIPTION)
    \relates <QtDlt>

    Defines a new QLoggingCategory \a CATEGORY and makes it configurable under the \a CATEGORYNAME identifier.
    The dlt context will be registered as \a DLT_CTX_NAME and using \a DLT_CTX_DESCRIPTION as a description.

    \note \a DLT_CTX_NAME is limited to (a size of) 4 characters.
*/

/*!
    \macro QDLT_REGISTER_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME, DLT_CTX_NAME, DLT_CTX_DESCRIPTION)
    \relates <QtDlt>

    Registers an existing QLoggingCategory \a CATEGORY configurable under the \a CATEGORYNAME identifier.
    The new category is mapped to the dlt context, \a DLT_CTX_NAME, with the \a DLT_CTX_DESCRIPTION.

    \note \a DLT_CTX_NAME is limited to (a size of) 4 characters.
*/

/*!
    \macro QDLT_FALLBACK_CATEGORY(CATEGORY)
    \relates <QtDlt>

    Registers an existing QLoggingCategory \a CATEGORY as the dlt fallback category. \a CATEGORY must be
    already mapped to a dlt context by using QDLT_LOGGING_CATEGORY or QDLT_REGISTER_LOGGING_CATEGORY. The fallback
    category is used for all log messages, which are not mapped to dlt contexts or don't use a QLoggingCategory.
*/

/*!
    \macro QDLT_REGISTER_CONTEXT_ON_FIRST_USE(ENABLED)
    \relates <QtDlt>

    When set to \a ENABLED the registration with the dlt-daemon is done on the first use of the category.

    Otherwise the registration is done directly when QDLT_LOGGING_CATEGORY or QDLT_REGISTER_LOGGING_CATEGORY
    is called.
*/

/*!
    \group qtdltlogging-examples
    \ingroup all-examples
    \title Qt DLT Logging Examples

    \brief Examples for the Qt DLT Logging module

    These are the Qt DLT Logging examples.
*/
