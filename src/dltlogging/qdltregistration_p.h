// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#include <QtDltLogging/private/qdltloggingglobal_p.h>
#include <QtDltLogging/qdltregistration.h>
#include <QtCore/QString>
#include <QtCore/QHash>
#include <QtCore/QMutex>

#include <dlt.h>

//
//  W A R N I N G
//  -------------
//
// This file is not part of the Qt API.  It exists purely as an
// implementation detail. This header file may change from version to
// version without notice, or even be removed.
//
// We mean it.
//

QT_BEGIN_NAMESPACE

void qtDltLogLevelChangedHandler(char context_id[], uint8_t log_level, uint8_t trace_status);

class QDltRegistrationPrivate
{
private:
    struct CategoryInfo {
        CategoryInfo() = default;

        QByteArray m_ctxName;
        QByteArray m_ctxDescription;
        QLoggingCategory *m_category = nullptr;
        DltContext *m_context = nullptr;
        bool m_registered = false;
    };

public:
    QDltRegistrationPrivate(QDltRegistration *parent);

    void registerCategory(const QLoggingCategory *category, DltContext *dltContext, const char *dltCtxName, const char *dltCtxDescription);
    void registerCategory(CategoryInfo &info);
    void unregisterCategories();
    void registerApplication();
    void unregisterApplication();
    void setDefaultCategory(const QString &category);

    DltContext *context(const char *categoryName);
    const QLoggingCategory *dltLogLevelChanged(char context_id[], uint8_t log_level, uint8_t trace_status);

    static DltLogLevelType category2dltLevel(const QLoggingCategory *category);
    static DltLogLevelType severity2dltLevel(QtMsgType type);

private:
    mutable QMutex m_mutex;
    QDltRegistration *const q_ptr;
    Q_DECLARE_PUBLIC(QDltRegistration)
    QString m_dltAppID;
    QString m_dltAppDescription;
    bool m_dltAppRegistered;
    QString m_defaultCategory;
    QHash<QString, CategoryInfo> m_categoryInfoHash;
    bool m_registerOnFirstUse;
    bool m_disabled;
    QDltRegistration::LongMessageBehavior m_longMessageBehavior;

    friend void qtDltLogLevelChangedHandler(char context_id[], uint8_t log_level, uint8_t trace_status);
};

QT_END_NAMESPACE
