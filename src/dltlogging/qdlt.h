// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef QDLT_H
#define QDLT_H

#if 0
#pragma qt_class(QtDlt)
#endif

#include <QtCore/QGlobalStatic>
#include <QtCore/QtGlobal>
#include <QtDltLogging/qdltregistration.h>

QT_BEGIN_NAMESPACE

#define QDLT_REGISTER_CONTEXT_ON_FIRST_USE(ENABLED) \
    QT_BEGIN_NAMESPACE \
    struct QDltRegistrator { \
        QDltRegistrator() { globalDltRegistration()->setRegisterContextOnFirstUseEnabled(ENABLED); } \
    }; \
    static QDltRegistrator qdltRegistrator; \
    QT_END_NAMESPACE \

#define QDLT_REGISTER_APPLICATION(APP, DESCRIPTION) \
QT_BEGIN_NAMESPACE \
struct QDltAppRegistrator { \
    QDltAppRegistrator() { globalDltRegistration()->registerApplication(APP, DESCRIPTION); } \
}; \
static QDltAppRegistrator qdltAppRegistrator; \
QT_END_NAMESPACE \


#define QDLT_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME, DLT_CTX_NAME, DLT_CTX_DESCRIPTION) \
Q_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME) \
QDLT_REGISTER_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME, DLT_CTX_NAME, DLT_CTX_DESCRIPTION) \

#define QDLT_REGISTER_LOGGING_CATEGORY(CATEGORY, CATEGORYNAME, DLT_CTX_NAME, DLT_CTX_DESCRIPTION) \
QT_BEGIN_NAMESPACE \
struct QDlt ## CATEGORY ## Registrator { \
    QDlt ## CATEGORY ## Registrator() { globalDltRegistration()->registerCategory(&CATEGORY() , DLT_CTX_NAME, DLT_CTX_DESCRIPTION); } \
}; \
static QDlt ## CATEGORY ## Registrator qdlt ## CATEGORY ## registrator; \
QT_END_NAMESPACE \

#define QDLT_FALLBACK_CATEGORY(CATEGORY) \
QT_BEGIN_NAMESPACE \
struct QDltDefaultRegistrator { \
    QDltDefaultRegistrator() { globalDltRegistration()->setDefaultContext(CATEGORY().categoryName()); } \
}; \
static QDltDefaultRegistrator qdltDefaultRegistrator; \
QT_END_NAMESPACE \

QT_END_NAMESPACE

#endif // QDLT_H

