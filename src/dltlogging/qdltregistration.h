// Copyright (C) 2021 The Qt Company Ltd.
// Copyright (C) 2019 Luxoft Sweden AB
// Copyright (C) 2018 Pelagicore AG
// SPDX-License-Identifier: LicenseRef-Qt-Commercial OR LGPL-3.0-only OR GPL-2.0-only OR GPL-3.0-only

#ifndef QDLTREGISTRATION_H
#define QDLTREGISTRATION_H

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtDltLogging/qdltloggingglobal.h>
#include <stdint.h>

QT_BEGIN_NAMESPACE

class QDltRegistrationPrivate;

class Q_DLTLOGGING_EXPORT QDltRegistration : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(QDltRegistration)

public:
    enum class LongMessageBehavior {
        Truncate,
        Split,
        Pass
    };
    Q_ENUM(LongMessageBehavior)

    Q_PROPERTY(LongMessageBehavior longMessageBehavior READ longMessageBehavior WRITE setLongMessageBehavior)

    QDltRegistration(QObject *parent = nullptr);
    ~QDltRegistration() override;

    void registerApplication(const char *dltAppID, const char *dltAppDescription);
    void unregisterApplication();

    void registerCategory(const QLoggingCategory *category, const char *dltCtxName, const char *dltCtxDescription);
    void unregisterCategories();
    void setDefaultContext(const char *categoryName);

    void setRegisterContextOnFirstUseEnabled(bool enabled);
    void registerUnregisteredContexts();

    void setLongMessageBehavior(LongMessageBehavior config);
    LongMessageBehavior longMessageBehavior() const;

    static void messageHandler(QtMsgType msgTypes, const QMessageLogContext &msgCtx, const QString &msg);

Q_SIGNALS:
    void logLevelChanged(const QLoggingCategory *category);

private:
    QDltRegistrationPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QDltRegistration)

    friend void qtDltLogLevelChangedHandler(char context_id[], uint8_t log_level, uint8_t trace_status);
};

Q_DLTLOGGING_EXPORT extern QDltRegistration *globalDltRegistration();

QT_END_NAMESPACE

#endif // QDLTREGISTRATION_H
